import kotlin.math.min

abstract class CreditCard(initLimit: Float) : BankCard() {
    override var ownBalance: Float = 0f
    private val creditLimit: Float = initLimit
    private var creditAmount: Float = initLimit

    override fun deposit(amountArg: Float) {
        val creditDelta: Float = min((creditLimit - creditAmount), amountArg)
        val ownDelta: Float = amountArg - creditDelta

        creditAmount += creditDelta
        ownBalance += ownDelta
    }

    override fun pay(amountArg: Float): Boolean {
        var amount = amountArg
        val isSuccess: Boolean
        val ownDelta: Float = min(ownBalance, amount)
        amount -= ownDelta

        if (creditAmount < amount) {
            println(" - Operation rejected (insufficient funds).\n")
            isSuccess = false
        } else {
            ownBalance -= ownDelta
            creditAmount -= amount
            println(" - Operation accepted.")
            isSuccess = true
        }
        return isSuccess
    }

    override fun showBalanceInfo() {
        println("Own Balance is: $ownBalance, Credit Amount is: $creditAmount.")
    }
}