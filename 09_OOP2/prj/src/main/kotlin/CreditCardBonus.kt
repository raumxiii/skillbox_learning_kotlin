import kotlin.math.min

class CreditCardBonus(initLimit: Float) : CreditCard(initLimit) {
    private var bonusAmount: Float = 0f

    private fun alterBonusAmount(bonusDeltaArg: Float): Boolean {
        val isSuccess: Boolean

        if (bonusAmount + bonusDeltaArg < 0) {
            println("Not enough Bonus\n")
            isSuccess = false
        } else {
            bonusAmount += bonusDeltaArg
            isSuccess = true
        }
        return isSuccess
    }

    override fun deposit(amountArg: Float) {
        val bonusDelta: Float = amountArg * bonusPercentCoeff

        super.deposit(amountArg)
        alterBonusAmount(bonusDelta)
        println("Bonuses accrued: +$bonusDelta")
        showAvailableFunds()
    }

    override fun pay(amountArg: Float): Boolean {
        return pay(amountArg = amountArg, useBonusArg = false)
    }

    fun pay(amountArg: Float, useBonusArg: Boolean = false): Boolean {
        var payAmount: Float = amountArg
        var isSuccess: Boolean = true
        var bonusDelta: Float = 0f

        print("Payment amount: -$payAmount")
        if (useBonusArg) {
            print(" bonus usage")
            bonusDelta = -min(payAmount, bonusAmount)
            payAmount += bonusDelta
            isSuccess = alterBonusAmount(bonusDelta)
        }

        if (isSuccess) {
            isSuccess = super.pay(payAmount)
        }
        if (isSuccess) {
            if (useBonusArg) {
                print("Bonuses used: $bonusDelta; ")
            }
            println("Payment amount from main Balance: -$payAmount.")
            showAvailableFunds()
        }

        return isSuccess
    }

    override fun showBonusRules() {
        println("Bonus percentage of receipts: $bonusPercent%.")
    }

    override fun showAvailableFunds() {
        super.showBalanceInfo()
        println("Bonus Amount is: $bonusAmount.\n")
    }

    companion object {
        private const val bonusPercent: Float = 5f
        private const val bonusPercentCoeff: Float = bonusPercent / 100
    }
}