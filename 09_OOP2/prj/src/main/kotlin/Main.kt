fun main() {
    val debitCardBonus: DebitCardBonus = DebitCardBonus(1000f)
    debitCardBonus.showBonusRules()
    debitCardBonus.showAvailableFunds()
    debitCardBonus.pay(100f)
    debitCardBonus.pay(1500f)
    debitCardBonus.deposit(1500f)
    debitCardBonus.pay(amountArg = 1500f, useBonusArg = true)
    println()

    val creditCardCashback: CreditCardCashback = CreditCardCashback(5000f)
    creditCardCashback.showBonusRules()
    creditCardCashback.showAvailableFunds()
    creditCardCashback.pay(100f)
    creditCardCashback.pay(1500f)
    creditCardCashback.pay(amountArg = 7000f)
    creditCardCashback.deposit(2000f)
    creditCardCashback.pay(amountArg = 3200f)
    println()

    val creditCardBonus: CreditCardBonus = CreditCardBonus(5000f)
    creditCardBonus.showBonusRules()
    creditCardBonus.showAvailableFunds()
    creditCardBonus.pay(100f)
    creditCardBonus.pay(1500f)
    creditCardBonus.pay(amountArg = 7000f)
    creditCardBonus.deposit(2000f)
    creditCardBonus.pay(amountArg = 3200f)
    creditCardBonus.pay(amountArg = 1500f, useBonusArg = true)

}
