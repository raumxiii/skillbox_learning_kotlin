class CreditCardCashback(initLimit: Float) : CreditCard(initLimit) {
    private fun payCashback(paymentAmountArg: Float) {
        val cashbackAmount: Float

        if (paymentAmountArg >= cashbackMinBuyAmount) {
            cashbackAmount = paymentAmountArg * cashbackPercentCoeff
            if (cashbackAmount > 0) {
                super.deposit(cashbackAmount)
                println("Cashback accrued: $cashbackAmount")
            }
        }
    }

    override fun pay(amountArg: Float): Boolean {
        print("Payment amount: -$amountArg")
        val isSuccess: Boolean = super.pay(amountArg)
        if (isSuccess) {
            payCashback(amountArg)
            showAvailableFunds()
        }
        return isSuccess
    }

    override fun deposit(amountArg: Float) {
        println("Deposit operation: +$amountArg")
        super.deposit(amountArg)
        showAvailableFunds()
    }

    override fun showAvailableFunds() {
        super.showBalanceInfo()
        println()
    }

    override fun showBonusRules() {
        println("Cashback percentage: $cashbackPercent%")
        println("Minimal payment for Cashback is: $cashbackMinBuyAmount.")
    }

    companion object {
        private const val cashbackMinBuyAmount: Float = 3000f
        private const val cashbackPercent: Float = 3f
        private const val cashbackPercentCoeff: Float = cashbackPercent / 100
    }
}