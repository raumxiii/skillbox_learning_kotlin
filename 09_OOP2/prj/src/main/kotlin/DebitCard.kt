abstract class DebitCard(initBalance: Float) : BankCard() {
    override var ownBalance: Float = initBalance

    override fun deposit(amountArg: Float) {
        println("Deposit operation: +$amountArg")
        ownBalance += amountArg
    }

    override fun showBalanceInfo() {
        println("Balance is: $ownBalance.")
    }

    override fun pay(amountArg: Float): Boolean {
        val isSuccess: Boolean
        if (ownBalance < amountArg) {
            println(" - Operation rejected (insufficient funds).\n")
            isSuccess = false
        } else {
            ownBalance -= amountArg
            println(" - Operation accepted.")
            isSuccess = true
        }
        return isSuccess
    }
}