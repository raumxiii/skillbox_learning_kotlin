abstract class BankCard {
    protected abstract var ownBalance: Float

    abstract fun deposit(amountArg: Float)
    abstract fun pay(amountArg: Float): Boolean
    abstract fun showBalanceInfo()
    abstract fun showAvailableFunds()
    abstract fun showBonusRules()
}
