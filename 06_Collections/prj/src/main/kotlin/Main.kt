fun main() {
    val phoneListSize: Int = getPhoneListSize()
    val phoneList: MutableList<String> = getPhoneList(phoneListSize)

    println("\nFull Phone List is: $phoneList")
    println("Phone List +7 is: " + phoneList.filter { it.substring(0, 2) == "+7" })

    val phoneSet = phoneList.toSet()
    println("Unique Phone count is: ${phoneSet.size}")
    println("Sum Length of the Phone List is: ${phoneList.sumOf { it.length }}\n")

    val phoneMap: MutableMap<String, String> = getPhoneMap(phoneSet)
    println("\nYour Phone List is:")
    phoneMap.toSortedMap(compareBy<String> { phoneMap[it] }.thenBy { it }).forEach { println("Abonent Name: ${it.value}, Phone Number: ${it.key}") }
}

fun getPhoneMap(phoneSet: Set<String>): MutableMap<String, String> {
    val resultMap: MutableMap<String, String> = mutableMapOf()

    phoneSet.forEach {
        var abonentName: String
        while (true) {
            print("Abonent Name for $it is ")
            abonentName = readLine().toString()
            if (abonentName.isBlank()) {
                print("You`re enter blank string, ")
            } else {
                break
            }
        }
        resultMap[it] = abonentName
    }
    return resultMap
}

fun getPhoneList(phoneListSize: Int): MutableList<String> {
    val resultList: MutableList<String> = mutableListOf()

    for (i in 0..phoneListSize - 1) {
        var phoneNumber: String
        while (true) {
            print("Please, Enter ${i + 1}st Phone Number: ")
            phoneNumber = readLine().toString()
            if (phoneNumber.isBlank()) {
                print("You`re enter blank string, ")
            } else {
                break
            }
        }
        resultList.add(i, phoneNumber)
    }

    return resultList
}

fun getPhoneListSize(): Int {
    var intVal: Int
    while (true) {
        print("Please, Enter Your positive Int number: ")
        intVal = readLine()?.toIntOrNull() ?: 0

        if (intVal > 0) break
        else println("Your number is incorrect.")
    }
    return intVal
}