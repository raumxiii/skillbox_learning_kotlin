object Weapons {
    fun createPistol(): AbstractWeapon {
        return object : AbstractWeapon(clipSize = 7, fireType = FireType.SingleShot) {}
    }

    fun createAutoPistol(): AbstractWeapon {
        return object : AbstractWeapon(clipSize = 14, fireType = FireType.CutOffFire(3)) {}
    }

    fun createRifle(): AbstractWeapon {
        return object : AbstractWeapon(clipSize = 5, fireType = FireType.SingleShot, ammoType = Ammo.SNIPER) {}
    }

    fun createAutoRifle(): AbstractWeapon {
        return object : AbstractWeapon(clipSize = 15, fireType = FireType.BurstFire(5), ammoType = Ammo.STANDARD) {}
    }

    fun createShotGun(): AbstractWeapon {
        return object : AbstractWeapon(clipSize = 2, fireType = FireType.CutOffFire(2), ammoType = Ammo.BUCKSHOT) {}
    }

    fun createMachineGun(): AbstractWeapon {
        return object : AbstractWeapon(clipSize = 45, fireType = FireType.BurstFire(15), ammoType = Ammo.STANDARD) {}
    }
}