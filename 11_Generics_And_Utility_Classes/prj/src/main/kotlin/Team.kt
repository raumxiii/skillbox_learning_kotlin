import kotlin.random.Random

class Team(teamSize: Int) {
    val team = arrayOfNulls<AbstractWarrior>(teamSize)

    init {
        for (i in 0 until teamSize) {
            team[i] = when (Random.nextInt(10)) {
                0 -> General()
                in 1..3 -> Captain()
                else -> Solder()
            }
        }
    }

    fun getNextWarrior(indexArg: Int): AbstractWarrior? {
        var isOver: Boolean = false
        var currentIndex: Int = indexArg

        while (!isOver) {
            if (!team[currentIndex]!!.isKilled)
                return team[currentIndex]

            if (currentIndex == team.size - 1)
                currentIndex = 0
            else
                currentIndex++

            if (currentIndex == indexArg)
                isOver = true
        }
        return null
    }
}