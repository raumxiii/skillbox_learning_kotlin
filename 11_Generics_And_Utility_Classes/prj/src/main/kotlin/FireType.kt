sealed class FireType {
    object SingleShot : FireType()
    data class BurstFire(val burstLength: Int) : FireType()
    data class CutOffFire(val burstLength: Int) : FireType()
}
