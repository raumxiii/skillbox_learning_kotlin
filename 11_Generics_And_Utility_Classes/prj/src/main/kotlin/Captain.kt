import kotlin.random.Random

class Captain : AbstractWarrior(
    120, 75, when (Random.nextInt(10)) {
        0 -> Weapons.createMachineGun()
        1 -> Weapons.createShotGun()
        2 -> Weapons.createRifle()
        4 -> Weapons.createPistol()
        5 -> Weapons.createAutoPistol()
        else -> Weapons.createAutoRifle()
    }
) {
    override val dodgeChance: Int = 40
}