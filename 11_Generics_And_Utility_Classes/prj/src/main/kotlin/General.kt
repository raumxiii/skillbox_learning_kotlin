import kotlin.random.Random

class General : AbstractWarrior(
    150, 95, when (Random.nextInt(10)) {
        in 0..1 -> Weapons.createMachineGun()
        in 2..4 -> Weapons.createPistol()
        else -> Weapons.createAutoPistol()
    }
) {
    override val dodgeChance: Int = 50
}