import kotlin.random.Random

abstract class AbstractWeapon(
    private val clipSize: Int,
    private val fireType: FireType,
    private val ammoType: Ammo? = null
) {
    private var clip: GenericStack<Ammo> = GenericStack()

    private fun createAmmo(ammoTypeArg: Ammo?): Ammo {
        val newAmmo: Ammo =
            when (ammoTypeArg) {
                Ammo.SNIPER -> Ammo.SNIPER
                Ammo.BUCKSHOT -> Ammo.BUCKSHOT
                Ammo.STANDARD -> Ammo.STANDARD
                else -> when (Random.nextInt(5)) {
                    0 -> Ammo.BUCKSHOT
                    1 -> Ammo.SNIPER
                    else -> Ammo.STANDARD
                }
            }
        return newAmmo
    }

    fun reload() {
        val newClip: GenericStack<Ammo> = GenericStack()

        for (i in 0 until clipSize) {
            newClip.push(createAmmo(ammoType))
        }
        clip = newClip
    }

    fun getFireAmmoPack(): List<Ammo> {
        val ammoPack: MutableList<Ammo> = mutableListOf()
        val shotBulletCount = when (fireType) {
            is FireType.SingleShot -> 1
            is FireType.CutOffFire -> fireType.burstLength
            is FireType.BurstFire -> maxOf(Random.nextInt(fireType.burstLength - 3, fireType.burstLength + 3), 3)
        }

        if (clip.remain() < shotBulletCount) throw NoAmmoException()

        for (i in 0 until shotBulletCount) {
            val ammo: Ammo = clip.pop()!!
            ammoPack.add(ammo)
        }

        return ammoPack
    }
}