import kotlin.random.Random

class Solder : AbstractWarrior(
    100, 60, when (Random.nextInt(10)) {
        0 -> Weapons.createMachineGun()
        1 -> Weapons.createShotGun()
        2 -> Weapons.createRifle()
        else -> Weapons.createAutoRifle()
    }
) {
    override val dodgeChance: Int = 35
}