enum class Ammo(private val damage: Int, private val critChance: Int, private val critCoeff: Int) {
    STANDARD(15, 25, 3),
    SNIPER(25, 50, 4),
    BUCKSHOT(25, 10, 2);

    fun calcDamage(): Int {
        return if (this.critChance.isSuccess())
            this.damage * this.critCoeff
        else
            this.damage
    }
}