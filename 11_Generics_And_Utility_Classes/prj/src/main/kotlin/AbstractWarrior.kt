abstract class AbstractWarrior(
    private val maxHitPoints: Int,
    private val accuracy: Int,
    private val weapon: AbstractWeapon
) : Warrior {
    override var isKilled: Boolean = false
    var hitPoints: Int = maxHitPoints

    override fun attack(targetWarrior: Warrior) {
        try {
            val fireAmmoPack = weapon.getFireAmmoPack()
            var totalDamage: Int = 0
            fireAmmoPack.forEach {
                if (accuracy.isSuccess() && !targetWarrior.dodgeChance.isSuccess())
                    totalDamage += it.calcDamage()
            }
            if (totalDamage > 0)
                targetWarrior.getDamage(totalDamage)
            else
                print("$this missed")
        } catch (e: NoAmmoException) {
            weapon.reload()
            print("$this reloading")
        }
        print(". ")
    }

    override fun getDamage(receivedDamage: Int) {
        hitPoints -= receivedDamage
        print("$this takes $receivedDamage damage, $hitPoints remain")
        if (hitPoints <= 0) {
            isKilled = true
            print("(dead)")
        }
    }
}