sealed class BattleState {
    object FirstTeamWin : BattleState()
    object SecondTeamWin : BattleState()
    object Draw : BattleState()
    object Progress : BattleState()
}
