interface Warrior {
    var isKilled: Boolean
    val dodgeChance: Int
    fun attack(targetWarrior: Warrior)
    fun getDamage(receivedDamage: Int)
}