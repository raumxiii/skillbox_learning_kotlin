import java.util.*

class GenericStack<T> {
    private var stack: Stack<T> = Stack<T>()

    fun push(itemArg: T) {
        stack.push(itemArg)
    }

    fun pop(): T? {
        return if (remain() > 0)
            stack.pop()
        else
            null
    }

    fun remain(): Int {
        return (stack.size)
    }
}