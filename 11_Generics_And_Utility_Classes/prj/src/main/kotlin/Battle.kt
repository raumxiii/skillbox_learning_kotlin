class Battle(teamSize: Int) {
    private val team1 = Team(teamSize)
    private val team2 = Team(teamSize)
    private var team1Remain: Int = teamSize
    private var team2Remain: Int = teamSize
    private var currentRound: Int = 1

    var isBattleOver: Boolean = false

    fun getBattleState() {
        val state: BattleState = when {
            !isBattleOver -> BattleState.Progress
            team1Remain > 0 -> BattleState.FirstTeamWin
            team2Remain > 0 -> BattleState.SecondTeamWin
            else -> BattleState.Draw
        }

        println()
        when (state) {
            BattleState.FirstTeamWin -> println("First Team Wins!")
            BattleState.SecondTeamWin -> println("Second Team Wins!")
            BattleState.Draw -> println("Both Teams are dead...")
            BattleState.Progress -> {
                println("Battle in progress: ")
                println("team One alive warriors:")
                team1.team.forEach { warrior ->
                    warrior?.let { if (!it.isKilled) println("$it - ${it.hitPoints} HP remains") }
                }
                println("team Two alive warriors:")
                team2.team.forEach { warrior ->
                    warrior?.let { if (!it.isKilled) println("$it - ${it.hitPoints} HP remains") }
                }
            }
        }
    }

    fun nextTurn() {
        println("\n$currentRound Round is coming!\n")
        for (c in 0 until maxOf(team1Remain, team2Remain)) {
            val team1Warrior: AbstractWarrior? = team1.getNextWarrior(c)
            val team2Warrior: AbstractWarrior? = team2.getNextWarrior(c)

            if (team1Warrior == null || team2Warrior == null)
                break
            println("$team1Warrior vs $team2Warrior")
            team1Warrior.attack(team2Warrior)
            team2Warrior.attack(team1Warrior)
            println()

            if (team1Warrior.isKilled)
                team1Remain--

            if (team2Warrior.isKilled)
                team2Remain--
        }

        if (team1Remain == 0 || team2Remain == 0)
            isBattleOver = true

        currentRound++
    }
}