import kotlin.random.Random

fun main() {
    var teamSize: Int
    var battle: Battle

    while (true) {
        print("Please, Enter size for Army Team: ")
        teamSize = readLine()?.toIntOrNull() ?: 0

        if (teamSize > 0) break
        else println("Incorrect Team size.")
    }

    battle = Battle(teamSize)

    while (!battle.isBattleOver) {
        battle.getBattleState()
        battle.nextTurn()
    }

    battle.getBattleState()
}

fun Int.isSuccess(): Boolean {
    val checkResult = Random.nextInt(100)
    return checkResult < this
}


class NoAmmoException : Throwable()