fun main() {
    println(decrypt("F2p)v\"y233{0->c}ttelciFc"))
}

fun decrypt(srcString : String) :String
{
    var leftPart : String = getPart(srcString)
    var rightPart : String = getPart(srcString, getLeft = false).reversed()

    leftPart = shift(leftPart, 1, stringFunc = {sourceString, intModificator -> getMap(sourceString, intModificator)})
    leftPart = leftPart.replace("5", "s")
    leftPart = leftPart.replace("4", "u")
    leftPart = shift(leftPart, -3, stringFunc = {sourceString, intModificator -> getMap(sourceString, intModificator)})
    leftPart = leftPart.replace("0", "o")

    rightPart = shift(rightPart, -4, stringFunc = {sourceString, intModificator -> getMap(sourceString, intModificator)})
    rightPart = rightPart.replace("_", " ")

    return  "$leftPart$rightPart"
}

fun shift(sourceString : String,
          intModificator : Int,
          stringFunc : (sourceString : String, intModificator : Int) -> String) : String
{
    return stringFunc(sourceString, intModificator)
}

fun getMap(sourceString : String, shiftNumber :Int) : String
{
    return sourceString.map{char -> char + shiftNumber}.joinToString("")
}

fun getPart(sourceString : String, getLeft : Boolean = true) : String
{
    val halfStringLength : Int = sourceString.length / 2

    return if (getLeft)
                sourceString.substring(0, halfStringLength)
           else
                sourceString.substring(halfStringLength)
}