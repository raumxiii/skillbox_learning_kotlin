import java.util.*
import kotlin.random.Random

class TrashBinClass(producedByArg: String) {
    val producedBy: String = producedByArg

    private var trashPile: Stack<TrashClass> = Stack<TrashClass>()

    private var fillLevel: Int = 0

    var isOpen: Boolean = false
        private set

    fun tryClose() {
        print("Try to Close: ")
        if (!isOpen)
            println("It`s Closed already")
        else if (fillLevel > capacity)
            println("It can`t be Closed because it`s overcrowded")
        else {
            isOpen = false
            println("Closed now")
        }
    }

    fun tryOpen() {
        print("Try to Open: ")
        if (isOpen)
            println("It`s Opened already")
        else {
            isOpen = true
            println("Opened now")
        }
    }

    fun putTrashIn() {
        print("Try to Put some trash in: ")
        if (!isOpen)
            tryOpen()

        if (fillLevel > capacity) {
            println("There is no more space")
            return
        }
        val newTrash: TrashClass = TrashPile.generateTrash()
        trashPile.push(newTrash)
        fillLevel += newTrash.volume
        println("${newTrash.name} was added to the top of pile")
    }

    fun putTrashOut(clearAll: Boolean = false) {
        print("Try to Put some trash out: ")
        if (!isOpen)
            tryOpen()

        var isComplete: Boolean = false
        if (trashPile.isEmpty())
            println("It`s Empty already")
        else {
            while (!isComplete) {
                val poppedTrash: TrashClass = trashPile.pop()
                fillLevel -= poppedTrash.volume
                println("${poppedTrash.name} was removed")

                if (!clearAll || trashPile.isEmpty())
                    isComplete = true
            }
        }
    }

    companion object {
        val capacity: Int = Random.nextInt(100 + 1)
    }
}