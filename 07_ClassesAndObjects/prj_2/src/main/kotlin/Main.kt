import kotlin.random.Random

fun main() {
    println("Hi! You`re lucky one - You`re have a Trash Bin!")

    var trashBinProducedBy: String
    while (true) {
        print("Please, tell us who produced this thing: ")
        trashBinProducedBy = readLine().toString()
        if (trashBinProducedBy.isBlank()) {
            print("You`ve entered blank string, ")
        } else {
            break
        }
    }

    val trashBin: TrashBinClass = TrashBinClass(trashBinProducedBy)
    println("Ok, now it`s a Trash Bin produced by ${trashBin.producedBy}! Let`s test it!")
    println("\nLet`s test it!")

    for (c in 0..Random.nextInt(4 + 1)) {
        if (Random.nextInt(2) == 1)
            trashBin.tryOpen()
        else
            trashBin.tryClose()
    }

    for (c in 0..Random.nextInt(10 + 1)) {
        trashBin.putTrashIn()
    }

    for (c in 0..Random.nextInt(3)) {
        if (Random.nextInt(2) == 1)
            trashBin.tryOpen()
        else
            trashBin.tryClose()
    }

    for (c in 0..Random.nextInt(10 + 1)) {
        trashBin.putTrashOut()
    }

    trashBin.tryClose()

    trashBin.putTrashOut(true)
    println("Test is Over. I Think you`ve enjoyed your pretty Trash Bin! Bye!")
}
