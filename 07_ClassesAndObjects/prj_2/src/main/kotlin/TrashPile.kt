import kotlin.random.Random

object TrashPile {
    private val trashPileContent = listOf<TrashClass>(
        TrashClass("Sock", 1),
        TrashClass("Dirty sock", 8),
        TrashClass("Dirty sock with hole", 7),
        TrashClass("Unlucky Charm", 13),
        TrashClass("World Balance", 69),
        TrashClass("Another Universe", 1000000000)
    )

    fun generateTrash(): TrashClass {
        return trashPileContent[Random.nextInt(trashPileContent.size)]
    }
}