class ChannelClass(nameArg: String, descriptionArg: String) {
    val name: String = nameArg
    val description: String = descriptionArg
}