object GlobalChannelList {
    private var channelList = listOf<ChannelClass>(
        ChannelClass("1st", "First Channel"),
        ChannelClass("2st", "Not First Channel"),
        ChannelClass("News", "Main News Channel"),
        ChannelClass("Comedy", "Humor Only! 24/7"),
        ChannelClass("National Geographic", "Channel About Geographic"),
        ChannelClass("Culture", "Channel About World Culture And History")
    )

    fun channelListSize(): Int {
        return channelList.size
    }

    fun getRandomChannelList(listSize: Int): List<ChannelClass> {
        channelList = channelList.shuffled()

        val resultChannelList: MutableList<ChannelClass> = mutableListOf()
        for (i in 0..listSize - 1) {
            resultChannelList.add(i, channelList[i])
        }

        return resultChannelList.toList()
    }
}