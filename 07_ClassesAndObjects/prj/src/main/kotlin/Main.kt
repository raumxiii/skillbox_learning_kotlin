import kotlin.random.Random

fun main() {
    val tvSetListSize = getTVListSize()
    val tvSet: MutableList<TVClass> = mutableListOf()

    for (i in 0..tvSetListSize - 1) {
        println("Please, enter primary properties for ${i + 1}st TV:")
        val producedBy: String = getStringVal("Production Firm")
        val model: String = getStringVal("model")
        val sizeArg: Int = getIntVal("screen size")
        val tV: TVClass = TVClass(producedBy, model, sizeArg)
        tvSet.add(i, tV)
        print("\n")
    }

    tvSet.forEach {
        diagnostic(it)
    }
}

fun getTVListSize(): Int {
    var listSize: Int
    while (true) {
        print("Please, Enter size of TV-list : ")
        listSize = readLine()?.toIntOrNull() ?: 0

        if (listSize > 0) break
        else println("Your number is incorrect.")
    }
    return listSize
}

fun getStringVal(valType: String): String {
    var stringValue: String
    while (true) {
        print("Please, enter the $valType: ")
        stringValue = readLine().toString()
        if (stringValue.isBlank()) {
            print("You`ve entered blank string, ")
        } else {
            break
        }
    }
    return stringValue
}

fun getIntVal(valType: String): Int {
    var intValue: Int
    while (true) {
        print("Please, enter $valType: ")
        intValue = readLine()?.toIntOrNull() ?: 0

        if (intValue > 0) break
        else print("You`ve entered incorrect value, ")
    }
    return intValue
}

fun diagnostic(tvForDiagnostic: TVClass) {
    println("producedBy: ${tvForDiagnostic.producedBy} Model: ${tvForDiagnostic.model} screen Size: ${tvForDiagnostic.screenSize}")

    if (tvForDiagnostic.isOn) {
        println("Current Channel is ${tvForDiagnostic.currentChannelNumber +1}")
        println("Current Volume is ${tvForDiagnostic.currentVolume}")
    } else println("Switched Off")

    for (i in 0..Random.nextInt(4)) tvForDiagnostic.switchOnOff(null)
    for (i in 0..Random.nextInt(15)) tvForDiagnostic.channelUp()
    for (i in 0..Random.nextInt(15)) tvForDiagnostic.channelDown()
    for (i in 0..Random.nextInt(4)) tvForDiagnostic.switchToChannel(Random.nextInt(10))
    for (i in 0..Random.nextInt(15)) tvForDiagnostic.volumeUp()
    for (i in 0..Random.nextInt(15)) tvForDiagnostic.volumeDown()
    println("diagnostic is complete\n")
}