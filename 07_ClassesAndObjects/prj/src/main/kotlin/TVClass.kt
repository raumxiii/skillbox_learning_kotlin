import kotlin.random.Random

class TVClass(producedByArg: String, modelArg: String, screenSizeArg: Int) {
    private val maxChannelSize: Int = Random.nextInt(GlobalChannelList.channelListSize()) + 1
    private val channelList: List<ChannelClass> = GlobalChannelList.getRandomChannelList(maxChannelSize)

    val producedBy: String = producedByArg
    val model: String = modelArg
    val screenSize: Int = screenSizeArg

    var currentVolume: Int = 0
        private set
    var currentChannelNumber: Int = 0
        private set
    var isOn: Boolean = false
        private set

    fun switchOnOff(bOn: Boolean?) {
        isOn = bOn ?: !isOn
        if (isOn) println("Switched On")
        else println("Switched Off")
    }

    fun switchToChannel(channelNumber: Int) {
        if (!isOn) switchOnOff(true)
        currentChannelNumber = minOf(maxOf(channelNumber, 0), channelList.size - 1)
        println("Switched to Channel №${channelNumber + 1}. Current Channel №${currentChannelNumber + 1} - ${channelList[currentChannelNumber].name} ")
    }

    fun channelUp() {
        if (!isOn) switchOnOff(true)
        else {
            currentChannelNumber = currentChannelNumber + 1
            if (currentChannelNumber > maxChannelSize - 1)
                currentChannelNumber = 0
        }
        println("channel Up. Current Channel №${currentChannelNumber + 1} - ${channelList[currentChannelNumber].name} ")
    }

    fun channelDown() {
        if (!isOn) switchOnOff(true)
        else {
            currentChannelNumber = currentChannelNumber - 1
            if (currentChannelNumber < 0)
                currentChannelNumber = maxChannelSize - 1
        }
        println("channel Down. Current Channel №${currentChannelNumber + 1} - ${channelList[currentChannelNumber].name} ")
    }

    fun volumeUp() {
        if (!isOn) switchOnOff(true)
        else {
            currentVolume = minOf(currentVolume + 1, maxVolume)
        }
        println("volume Up. Current Value Level is ${currentVolume}")
    }

    fun volumeDown() {
        if (!isOn) switchOnOff(true)
        else {
            currentVolume = maxOf(currentVolume - 1, 0)
        }
        println("volume Down. Current Value Level is ${currentVolume}")
    }

    companion object {
        const val maxVolume = 100
    }
}