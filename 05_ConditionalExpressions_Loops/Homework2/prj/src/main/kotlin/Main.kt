import kotlin.math.absoluteValue

fun main() {
    val intA: Int = getNum()
    val lastFbNum: Int = getLastFbNum(intA)
    println("The Last Fibonacci number is $lastFbNum")
}

fun getNum(): Int {
    var intA: Int
    while (true) {
        print("Please, Enter Your positive Int number: ")
        intA = readLine()?.toIntOrNull() ?: 0

        if (intA > 0) break
        else println("Your number is incorrect.")
    }
    return intA
}

fun getLastFbNum(srcNumber: Int): Int {
    var resultNumber: Int = 0
    var prev1Number: Int = if (srcNumber == 0) 1
                           else srcNumber / srcNumber.absoluteValue
    var prev2Number: Int = prev1Number

    when (srcNumber.absoluteValue) {
        0 -> resultNumber = 0
        in (1..2) -> resultNumber = prev1Number
        else -> {
            for (c in 0..srcNumber.absoluteValue - 3) {
                resultNumber = prev1Number + prev2Number
                prev1Number = prev2Number
                prev2Number = resultNumber
            }
        }
    }
    return resultNumber
}