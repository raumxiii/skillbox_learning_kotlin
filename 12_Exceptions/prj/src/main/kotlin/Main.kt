import kotlin.random.Random

fun main() {
    val wheel = Wheel(Random.nextInt(1, 10) + Random.nextFloat())
    try {
        while (true) {
            try {
                wheel.checkPressure()
                break
            } catch (e: TooLowPressure) {
                wheel.pump(Random.nextInt(10) + Random.nextFloat())
            } catch (e: TooHighPressure) {
                wheel.release(Random.nextInt(10) + Random.nextFloat())
            }
        }
        println("It`s good Pressure - Let`s GO!")
    } catch (e: DestroyedWheel) {
        println(e.message)
    }
}

class TooHighPressure : Throwable("Pressure is too high")
class TooLowPressure : Throwable("Pressure is too low")
class DestroyedWheel : Throwable("It can`t be used - it`s Destroyed completely")