class Wheel(private var pressure: Float) {
    private fun alterPressure(pressureDelta: Float) {
        val newPressure = pressure + pressureDelta

        pressure = maxOf(newPressure, minPressure)
        println("current pressure is $pressure")
        if (pressure > maxPressure) throw DestroyedWheel()
    }

    fun pump(pressureDelta: Float) {
        alterPressure(pressureDelta)
    }

    fun release(pressureDelta: Float) {
        alterPressure(-pressureDelta)
    }

    fun checkPressure() {
        when {
            pressure > maxWorkPressure -> throw TooHighPressure()
            pressure < minWorkPressure -> throw TooLowPressure()
        }
    }

    companion object {
        const val minPressure: Float = 0f
        const val maxPressure: Float = 10f
        const val minWorkPressure: Float = 1.6f
        const val maxWorkPressure: Float = 2.5f
    }
}