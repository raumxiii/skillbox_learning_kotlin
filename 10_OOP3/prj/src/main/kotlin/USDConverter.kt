import kotlin.math.roundToInt

class USDConverter : CurrencyConverter {
    override val currencyCode: String = "USD"
    override val convertRate: Double = 103.16
}