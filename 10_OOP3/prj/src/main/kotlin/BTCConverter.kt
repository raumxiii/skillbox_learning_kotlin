import kotlin.math.roundToInt

class BTCConverter : CurrencyConverter {
    override val currencyCode: String = "BTC"
    override val convertRate: Double = 4430807.56
}