import kotlin.math.roundToInt

interface CurrencyConverter {
    val currencyCode: String
    val convertRate: Double
    fun convertToRub(curAmountArg: Int): Int {
        return (curAmountArg * convertRate).roundToInt()
    }

    fun convertFromRub(curAmountArg: Int): Int {
        return (curAmountArg / convertRate).roundToInt()
    }
}