import kotlin.math.roundToInt

class EURConverter : CurrencyConverter {
    override val currencyCode: String = "EUR"
    override val convertRate: Double = 113.26
}