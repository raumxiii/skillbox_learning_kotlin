fun main() {
    val converterList = mutableListOf(
        ConverterList.get("EUR"),
        ConverterList.get("USD"),
        ConverterList.get("BTC"),
        ConverterList.get("DZD"),
        ConverterList.get("BGN"),
        ConverterList.get("GHS")
    )

    var rubAmount: Int = 1000

    converterList.forEach { println("${it.currencyCode} Rate is ${it.convertRate}") }
    println()

    converterList.forEach { println("$rubAmount rub = ${it.convertFromRub(rubAmount)} ${it.currencyCode}") }
    println()

    rubAmount = 500
    converterList.forEach { println("$rubAmount rub = ${it.convertFromRub(rubAmount)} ${it.currencyCode}") }
    println()

    rubAmount = 1
    converterList.forEach { println("$rubAmount rub = ${it.convertFromRub(rubAmount)} ${it.currencyCode}") }
    println()

    rubAmount = 10000000
    converterList.forEach { println("$rubAmount rub = ${it.convertFromRub(rubAmount)} ${it.currencyCode}") }
}