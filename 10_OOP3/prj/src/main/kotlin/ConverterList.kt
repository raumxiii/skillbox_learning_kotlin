import kotlin.random.Random

object ConverterList {
    private var converterMap = mutableMapOf(
        "USD" to USDConverter(),
        "EUR" to EURConverter(),
        "BTC" to BTCConverter()
    )

    fun get(currencyCodeArg: String): CurrencyConverter {
        if (!converterMap.containsKey(currencyCodeArg)) {
            converterMap[currencyCodeArg] = object : CurrencyConverter {
                override val currencyCode: String = currencyCodeArg
                override val convertRate: Double = (Random.nextInt(1000) + (Random.nextInt(100).toFloat() * 0.01))
            }
        }
        return converterMap[currencyCodeArg]!!
    }
}