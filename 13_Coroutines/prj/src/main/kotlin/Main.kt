import kotlinx.coroutines.*
import java.math.BigInteger

fun main() {
    runBlocking {
        val var1 = async { Fibonacci.take(300001) }
        val var2 = async { Fibonacci.take(51) }
        val var3 = async { Fibonacci.take(100001) }
        val var4 = async { Fibonacci.take(101) }

        val jobDot = launch {
            while (true) {
                yield()
                print(".")
                delay(100)
            }
        }

        var1.join()
        var2.join()
        var3.join()
        var4.join()
        jobDot.cancelAndJoin()

        println("\nvar1 = ${var1.await()};\nvar2 = ${var2.await()};\nvar3 = ${var3.await()};\nvar4 = ${var4.await()}")
    }
}

fun Int.toBigInteger(): BigInteger {
    return BigInteger.valueOf(this.toLong())
}