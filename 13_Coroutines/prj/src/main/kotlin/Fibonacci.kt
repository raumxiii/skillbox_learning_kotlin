import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.delay
import kotlinx.coroutines.withTimeout
import kotlinx.coroutines.yield
import java.math.BigInteger
import kotlin.math.absoluteValue

object Fibonacci {
    suspend fun take(sequenceNumber: Int): BigInteger? {
        val initVal: Int = 0
        var firstNumber: BigInteger = initVal.toBigInteger()
        var secondNumber: BigInteger = firstNumber
        var resultNumber: BigInteger = firstNumber
        try {
            when {
                sequenceNumber > 0 -> secondNumber++
                sequenceNumber < 0 -> secondNumber--
            }
            withTimeout(1500) {
                for (c in 0..sequenceNumber.absoluteValue - 2) {
                    yield()
                    resultNumber = firstNumber + secondNumber
                    firstNumber = secondNumber
                    secondNumber = resultNumber
                }
            }
            return resultNumber
        } catch (e: TimeoutCancellationException) {
            println("\nthe value of $sequenceNumber could not be calculated in the allowed time")
            return null
        }
    }
}