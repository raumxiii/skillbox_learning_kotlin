import kotlin.random.Random

fun main() {
    val zoo: NaturalReserve =
        NaturalReserve(birdCountArg = 5, fishCountArg = 3, dogCountArg = 2, otherCountArg = Random.nextInt(5))

    zoo.process(Random.nextInt(5, 10))
}