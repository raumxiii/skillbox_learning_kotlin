class Fish(firstNameArg: String, maxAgeArg: Int, lastNameArg: String = "") :
    Animal(firstNameArg = firstNameArg, maxAgeArg = maxAgeArg, lastNameArg = lastNameArg) {

    override fun move() {
        if (isTooOld()) {
            println("$name is too old to swim")
            return
        }
        if (isTooTired(moveEnergyDelta)) {
            println("$name is too tired to swim")
            return
        }
        if (isTooSkinny(moveWeightDelta)) {
            println("$name is too skinny to swim")
            return
        }

        println("$name is swimming")
        tryIncrementAge(deltaAgeArg = moveAgeDelta)
        tryIncrementEnergy(moveEnergyDelta)
        tryIncrementWeight(moveWeightDelta)
    }

    override fun makeChild(): Fish {
        val childAnimal = Fish(firstNameArg = NameDict.getName("fish"), maxAgeArg = maxAge, lastNameArg = firstName)
        childAnimal.describe()
        return childAnimal
    }
}