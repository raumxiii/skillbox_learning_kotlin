class Bird(firstNameArg: String, maxAgeArg: Int, lastNameArg: String = "") :
    Animal(firstNameArg = firstNameArg, maxAgeArg = maxAgeArg, lastNameArg = lastNameArg) {

    override fun move() {
        if (isTooOld()) {
            println("$name is too old to fly")
            return
        }
        if (isTooTired(moveEnergyDelta)) {
            println("$name is too tired to fly")
            return
        }
        if (isTooSkinny(moveWeightDelta)) {
            println("$name is too skinny to fly")
            return
        }

        println("$name is flying")
        tryIncrementAge(deltaAgeArg = moveAgeDelta)
        tryIncrementEnergy(moveEnergyDelta)
        tryIncrementWeight(moveWeightDelta)
    }

    override fun makeChild(): Bird {
        val childAnimal = Bird(firstNameArg = NameDict.getName("bird"), maxAgeArg = maxAge, lastNameArg = firstName)
        childAnimal.describe()
        return childAnimal
    }
}