class Dog(firstNameArg: String, maxAgeArg: Int, lastNameArg: String = "") :
    Animal(firstNameArg = firstNameArg, maxAgeArg = maxAgeArg, lastNameArg = lastNameArg) {

    override fun move() {
        if (isTooOld()) {
            println("$name is too old to run")
            return
        }
        if (isTooTired(moveEnergyDelta)) {
            println("$name is too tired to run")
            return
        }
        if (isTooSkinny(moveWeightDelta)) {
            println("$name is too skinny to run")
            return
        }

        println("$name is running")
        tryIncrementAge(deltaAgeArg = moveAgeDelta)
        tryIncrementEnergy(moveEnergyDelta)
        tryIncrementWeight(moveWeightDelta)
    }

    override fun makeChild(): Dog {
        val childAnimal = Dog(firstNameArg = NameDict.getName("dog"), maxAgeArg = maxAge, lastNameArg = firstName)
        childAnimal.describe()
        return childAnimal
    }
}