import kotlin.random.Random

class NaturalReserve(birdCountArg: Int, fishCountArg: Int, dogCountArg: Int, otherCountArg: Int) {
    private val animalList: MutableList<Animal> = mutableListOf()
    private var deadCount: Int = 0
    private var birthCount: Int = 0

    init {
        for (i in 0..birdCountArg) {
            animalList.add(
                Bird(
                    firstNameArg = NameDict.getName("bird"),
                    maxAgeArg = Random.nextInt(birdMinAge, birdMaxAge + 1)
                )
            )
        }
        for (i in 0..fishCountArg) {
            animalList.add(
                Fish(
                    firstNameArg = NameDict.getName("fish"),
                    maxAgeArg = Random.nextInt(fishMinAge, fishMaxAge + 1)
                )
            )
        }
        for (i in 0..dogCountArg) {
            animalList.add(
                Dog(
                    firstNameArg = NameDict.getName("dog"),
                    maxAgeArg = Random.nextInt(dogMinAge, dogMaxAge + 1)
                )
            )
        }
        for (i in 0..otherCountArg) {
            animalList.add(
                Animal(
                    firstNameArg = NameDict.getName(),
                    maxAgeArg = Random.nextInt(animalMinAge, animalMaxAge + 1)
                )
            )
        }
    }

    private fun lifeCycle() {
        val newAnimalList: MutableList<Animal> = mutableListOf()
        deadCount = 0
        birthCount = 0

        with(animalList.iterator())
        {
            forEach {
                val action = actionMap[Random.nextInt(4)]
                println("\n${it.name} trying to $action:")
                when (action) {
                    "eat" -> it.eat()
                    "move" -> it.move()
                    "sleep" -> it.sleep()
                    "make Child" -> {
                        val newAnimal: Animal = it.makeChild()
                        println("Welcome, ${newAnimal.name}!")
                        newAnimalList.add(newAnimal)
                        birthCount += 1
                    }
                }
                if (it.isTooOld()) {
                    println("${it.name} is too old. It`s dead...")
                    deadCount += 1
                    remove()
                }
            }
        }

        if (newAnimalList.isNotEmpty())
            animalList.addAll(newAnimalList)
    }

    fun process(cycleCountArg: Int) {
        for (i in 0..cycleCountArg) {
            println("\n${i + 1}`st Day is Coming!")
            lifeCycle()
            if (animalList.isEmpty()) {
                println("And there is no one left here... We`re closed...")
                break
            } else {
                println("\nIt`s ${animalList.size} animals here: ${animalList.count { it is Bird }} birds, " +
                        "${animalList.count { it is Dog }} dogs, ${animalList.count { it is Fish }} fishes, " +
                        "${animalList.count { !(it is Bird || it is Dog || it is Fish) }} others." +
                        "\n$birthCount was born and $deadCount passed away. We`ll Waiting for you tomorrow!"
                )
            }
        }
    }

    private companion object {
        const val birdMinAge: Int = 3
        const val birdMaxAge: Int = 10

        const val fishMinAge: Int = 2
        const val fishMaxAge: Int = 8

        const val dogMinAge: Int = 7
        const val dogMaxAge: Int = 15

        const val animalMinAge: Int = 5
        const val animalMaxAge: Int = 12

        val actionMap = mapOf(
            0 to "move",
            1 to "eat",
            2 to "sleep",
            3 to "make Child"
        )
    }
}