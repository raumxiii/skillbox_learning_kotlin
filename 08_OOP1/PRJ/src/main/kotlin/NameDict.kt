import kotlin.random.Random

object NameDict {
    private val fishNameDict = arrayOf("BobFish", "MaryFish", "JannyFish", "MarkFish", "AntonyFish")
    private val dogNameDict = arrayOf("BobDog", "MaryDog", "JannyDog", "MarkDog", "AntonyDog")
    private val birdNameDict = arrayOf("BobBird", "MaryBird", "JannyBird", "MarkBird", "AntonyBird")
    private val nameDict = arrayOf("Bob", "Mary", "Janny", "Mark", "Antony")

    fun getName(animalType: String = "animal"): String {
        val resultName: String = when (animalType.lowercase()) {
            "fish" -> fishNameDict[Random.nextInt(fishNameDict.size)]
            "dog" -> dogNameDict[Random.nextInt(dogNameDict.size)]
            "bird" -> birdNameDict[Random.nextInt(birdNameDict.size)]
            else -> nameDict[Random.nextInt(nameDict.size)]
        } ?: "NoName"

        return resultName
    }
}