import kotlin.random.Random

open class Animal(
    firstNameArg: String,
    maxAgeArg: Int,
    lastNameArg: String = "",
    initialEnergyArg: Int = Random.nextInt(10) + 1,
    initialWeightArg: Int = Random.nextInt(5) + 1
) {
    private var age: Int = 0
    private var energy: Int = initialEnergyArg
    private var weight: Int = initialWeightArg
    private val lastName: String = lastNameArg

    protected val firstName: String = firstNameArg
    protected val maxAge: Int = maxAgeArg
    val name: String = ("$firstName $lastName").trim()

    protected fun isTooTired(deltaEnergyArg: Int): Boolean {
        return 0 > energy + deltaEnergyArg
    }

    protected fun isTooSkinny(deltaWeightArg: Int): Boolean {
        return 1 > weight + deltaWeightArg
    }

    protected fun tryIncrementAge(deltaAgeArg: Int, noRandom: Boolean = false) {
        if (noRandom || Random.nextBoolean())
            age += deltaAgeArg
    }

    protected fun tryIncrementEnergy(deltaEnergyArg: Int) {
        energy += deltaEnergyArg
        energy = maxOf(energy, 0)
    }

    protected fun tryIncrementWeight(deltaWeightArg: Int) {
        weight += deltaWeightArg
        weight = maxOf(weight, 0)
    }

    protected fun describe() {
        println("Name = $name, age = $age, maxAge = $maxAge, energy = $energy, weight = $weight")
    }

    fun isTooOld(): Boolean {
        return age >= maxAge
    }

    fun sleep() {
        if (isTooOld()) {
            println("$name is too old to sleep")
            return
        }

        println("$name is sleeping")
        tryIncrementAge(deltaAgeArg = sleepAgeDelta, noRandom = true)
        tryIncrementEnergy(sleepEnergyDelta)
    }

    fun eat() {
        if (isTooOld()) {
            println("$name  is too old to eat")
            return
        }
        println("$name is eating")
        tryIncrementAge(deltaAgeArg = eatAgeDelta)
        tryIncrementEnergy(eatEnergyDelta)
        tryIncrementWeight(eatWeightDelta)
    }

    open fun move() {
        if (isTooOld()) {
            println("$name is too old to move")
            return
        }
        if (isTooTired(moveEnergyDelta)) {
            println("$name is too tired to move")
            return
        }
        if (isTooSkinny(moveWeightDelta)) {
            println("$name is too skinny to move")
            return
        }

        println("$name is moving")
        tryIncrementAge(deltaAgeArg = moveAgeDelta)
        tryIncrementEnergy(moveEnergyDelta)
        tryIncrementWeight(moveWeightDelta)
    }

    open fun makeChild(): Animal {
        val childAnimal = Animal(firstNameArg = NameDict.getName(), maxAgeArg = maxAge, lastNameArg = firstName)
        childAnimal.describe()
        return childAnimal
    }

    companion object {
        const val sleepAgeDelta: Int = 1
        const val sleepEnergyDelta: Int = 5

        const val eatAgeDelta: Int = 1
        const val eatEnergyDelta: Int = 3
        const val eatWeightDelta: Int = 1

        const val moveAgeDelta: Int = 1
        const val moveEnergyDelta: Int = -5
        const val moveWeightDelta: Int = -1
    }
}