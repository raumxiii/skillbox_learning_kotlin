suspend fun main() {
    Game.play()
}

class NoMoreCubesException : Throwable("There is no more Cubes!")