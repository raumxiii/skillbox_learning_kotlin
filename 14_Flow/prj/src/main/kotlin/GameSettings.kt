object GameSettings {
    // Минимальное значение бочонка
    private const val minCubeVal = 1

    // Максимальное значение бочонка
    private const val maxCubeVal = 90

    // Число строк. Свыше 10 не рекомендуется использовать - можно улететь в бесконечный цикл
    private const val rowCount = 3

    // Число билетов на руки одному игроку
    private const val maxCardCount = 2

    // Победа только при полном заполнении билета
    private const val fullFieldVictory = false

    // Получить минимальное значение бочонка
    fun minCubeVal(): Int {
        return minCubeVal
    }

    // Получить максимальное значение бочонка
    fun maxCubeVal(): Int {
        return maxCubeVal
    }

    // Получить число строк в билете
    fun getRowCount(): Int {
        return rowCount
    }

    // Получить число билетов на руки одному игроку
    fun getMaxCardCount(): Int {
        return maxCardCount
    }

    // Получить число колонок в билете
    fun getColCount(): Int {
        return maxOf(maxCubeVal / 10, 1)
    }

    /* Получение числа полей, необходимых для заполнения в одной строке
     * В одной строке не может быть заполнено больше половины полей
     */
    fun getFieldColCount(): Int {
        val shift: Int = 1 - getColCount() % 2
        return maxOf(getColCount() / 2 - shift, 1)
    }

    // Длинна ячейки при отрисовке билета
    fun getPadNum(): Int {
        return maxCubeVal.toString().length + 2
    }

    // Победа только при полном заполнении билета
    fun isFullFieldVictory(): Boolean {
        return fullFieldVictory
    }
}