sealed class GameStatusList {
    object InProgress : GameStatusList()
    object Stopped : GameStatusList()
    data class Victory(val cardNumber1: Int, val cardNumber2: Int) : GameStatusList()

    fun getVictoryCardNum(): Pair<Int, Int> {
        return when (this) {
            is Victory -> Pair(cardNumber1, cardNumber2)
            else -> Pair(-1, -1)
        }
    }
}
