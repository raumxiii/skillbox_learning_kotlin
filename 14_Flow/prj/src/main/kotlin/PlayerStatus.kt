sealed class PlayerStatus {
    object InProgress : PlayerStatus()
    data class Finished(val cardNumber: Int) : PlayerStatus()

    fun getVictoryCardNum(): Int {
        return when (this) {
            is InProgress -> -1
            is Finished -> cardNumber
        }
    }
}
