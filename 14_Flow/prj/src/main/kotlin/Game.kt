import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.takeWhile
import kotlinx.coroutines.flow.zip
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

object Game {
    private val player1 = Player("Player 1")
    private val player2 = Player("Player 2")

    suspend fun play() {
        print("${player1.playerName}`s card list:")
        player1.showCardList()

        print("\n${player2.playerName}`s card list:")
        player2.showCardList()

        println("\nCube History:")
        runBlocking {
            launch {

                // Чтение статуса обоих игроков в потоке до тех пор, пока кто-то не победит, или не закончатся кубики
                player1.statusFlow()
                    .zip(player2.statusFlow()) { playerStatus1, playerStatus2 ->
                        when {
                            playerStatus1 == PlayerStatus.InProgress && playerStatus2 == PlayerStatus.InProgress -> GameStatusList.InProgress
                            else -> GameStatusList.Victory(
                                playerStatus1.getVictoryCardNum(),
                                playerStatus2.getVictoryCardNum()
                            )
                        }
                    }.takeWhile { GameStatus.currentState == GameStatusList.InProgress }
                    .collect {
                        GameStatus.currentState = it
                    }

                //Обработка завершения игры
                println("\n\nGame Finished!")
                val statePair = GameStatus.currentState.getVictoryCardNum()

                when {
                    GameStatus.currentState == GameStatusList.Stopped -> println("Game Was Stopped")
                    statePair.first != -1 && statePair.second != -1 -> println("Both Players Wins!")
                    statePair.first != -1 -> println("${player1.playerName} Win!")
                    statePair.second != -1 -> println("${player2.playerName} Win!")
                }

                if (statePair.first != -1) {
                    print("${player1.playerName} Victory Card:")
                    player1.showCard(statePair.first)
                }
                if (statePair.second != -1) {
                    print("${player2.playerName} Victory Card:")
                    player2.showCard(statePair.second)
                }

                // Завершение основного процесса
                this.cancel()
            }
        }
    }
}