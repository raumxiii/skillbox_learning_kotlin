import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import kotlin.random.Random


object GameHost {
    private val passedCubeList = mutableListOf<Int>()
    private val scope = CoroutineScope(Job() + Dispatchers.Default)
    private var cubeCount = 1

    // Функция генерации уникального кубика.
    private fun generateCube(): Int {
        while (true) {
            val newCube = Random.nextInt(GameSettings.minCubeVal(), GameSettings.maxCubeVal() + 1)
            when {
                !passedCubeList.contains(newCube) -> {
                    passedCubeList.add(newCube)
                    return newCube
                }
                passedCubeList.count() == GameSettings.maxCubeVal() - GameSettings.minCubeVal() -> throw NoMoreCubesException()
                else -> null
            }
        }
    }

    // Горячий поток генерации кубиков до тех пор, пока есть, хоть один подписчик.
    val cubeGeneratorFlow = flow {
        while (GameStatus.currentState == GameStatusList.InProgress) {
            try {
                val newCube = generateCube()
                if (cubeCount % 10 == 0)
                    println("$newCube")
                else
                    print("$newCube ")
                emit(newCube)
                cubeCount++
            } catch (e: NoMoreCubesException) {
                println(e.message)
                GameStatus.currentState = GameStatusList.Stopped
                emit(-1)
            }
            delay(500)
        }
        emit(-1)
    }.shareIn(scope = scope, started = SharingStarted.WhileSubscribed(), replay = 1)

}