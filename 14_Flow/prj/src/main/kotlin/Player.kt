import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.takeWhile

class Player(val playerName: String) {
    private var cardList = arrayListOf<LottoCard>()
    private var status: PlayerStatus = PlayerStatus.InProgress

    init {
        for (c in 0 until GameSettings.getMaxCardCount())
            cardList += LottoCard()
    }

    private fun markList(cubeValue: Int) {
        for (c in 0 until cardList.size)
            cardList[c].markField(cubeValue)
    }

    fun showCard(cardNum: Int) {
        if (cardNum >= 0 && cardNum < cardList.size) {
            println("\nCard ${cardNum + 1}:")
            cardList[cardNum].show()
        } else
            println("\nCard ${cardNum + 1} is not exists")
    }

    fun showCardList() {
        for (c in 0 until cardList.size) {
            println("\nCard ${c + 1}:")
            cardList[c].show()
        }
    }

    /* Чтение потока кубов и отметка в игровых карточках.
     * Чтение происходит только, если игра еще в статусе "в процессе".
     * При завершении игры, возвращается итоговый статус игрока (необходимо, если закончатся кубики)
     */
    fun statusFlow(): Flow<PlayerStatus> = flow {
        GameHost.cubeGeneratorFlow.takeWhile { GameStatus.currentState == GameStatusList.InProgress }.collect {
            markList(it)
            for (c in 0 until cardList.size) {
                if (!GameSettings.isFullFieldVictory() && cardList[c].isCompleteAnyRow() || cardList[c].isCompleteFullField()) {
                    status = PlayerStatus.Finished(c)
                    break
                }
            }
            emit(status)
        }
        emit(status)
    }
}