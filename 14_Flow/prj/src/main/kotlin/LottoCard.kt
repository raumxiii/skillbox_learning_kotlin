import kotlin.random.Random

class LottoCard {
    private var numberField = Array(GameSettings.getRowCount()) { mutableMapOf<Int, Char>() }

    init {
        for (i in 0 until GameSettings.getRowCount()) {
            for (j in 0..GameSettings.getFieldColCount()) {
                var isOk = false
                var newKey = 0
                while (!isOk) {
                    newKey = Random.nextInt(
                        maxOf((j * 2 * 10) + 1, GameSettings.minCubeVal()),
                        minOf(((j * 2 + 1) * 10) + 11, GameSettings.maxCubeVal() + 1)
                    )

                    isOk = true
                    for (k in 0 until i) {
                        if (numberField[k].containsKey(newKey))
                            isOk = false
                    }
                }
                numberField[i][newKey] = ' '
            }
        }
    }

    // Отметка на билете, в случае совпадения
    fun markField(cubeValue: Int) {
        for (c in 0 until GameSettings.getRowCount())
            if (numberField[c].containsKey(cubeValue)) {
                numberField[c][cubeValue] = 'X'
                break
            }
    }

    // Отрисовка билета
    fun show() {
        for (k in 0 until GameSettings.getRowCount()) {
            val rowArray = arrayOfNulls<Int>(GameSettings.getColCount())

            numberField[k].forEach {
                var rowPosition = it.key / 10
                if (it.key % 10 == 0)
                    rowPosition--
                rowArray[rowPosition] = it.key
            }

            for (c in 0 until GameSettings.getColCount()) {
                print("|")
                val cellContainer: String
                if (rowArray[c] == null)
                    cellContainer = ""
                else
                    cellContainer = rowArray[c].toString() + " " + numberField[k][rowArray[c]].toString()

                print(cellContainer.padStart(GameSettings.getPadNum(), ' '))
            }
            println("|")
        }
    }

    // Проверить факт полного успешного заполнения конкретной строки
    private fun isCompleteRow(rowNumber: Int): Boolean {
        var result = false

        if (rowNumber < GameSettings.getRowCount())
            result = !numberField[rowNumber].containsValue(' ')
        return result
    }

    // Проверить факт полного успешного заполнения хотя бы одной строки
    fun isCompleteAnyRow(): Boolean {
        var result = false
        for (c in 0 until GameSettings.getRowCount())
            if (isCompleteRow(c)) {
                result = true
                break
            }
        return result
    }

    // Проверить факт заполнения всех строк в карточке
    fun isCompleteFullField(): Boolean {
        var result = true
        for (c in 0 until GameSettings.getRowCount())
            if (!isCompleteRow(c)) {
                result = false
                break
            }
        return result
    }

}